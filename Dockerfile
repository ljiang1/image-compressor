FROM node:12-alpine

RUN apk update
RUN apk add --no-cache git autoconf libtool nasm libpng-dev automake pkgconfig build-base

CMD ["node"]
