# Image Compressor

## Run in Docker container

The Docker container will be built from a node alpine Docker image with minimal system setup. 

- To run the Docker container: `docker-compose up -d`
- To tear down the Docker container: `docker-compose down -v`

## Run script

All the script files are located in directory `lib`. The script will take all the assets from source directory `lib/src` and export all compressed assets to destination directory `lib/dest`. To run the script:

- Install node packages: `yarn install`
- Run the script: `yarn run compress`

## References

Referring to the following articles to compress images (collectively for formats png, jpg, svg and gif), default settings from various plugins of the node module imagemin are used.

- [Web.dev - Use optimized images](https://web.dev/uses-optimized-images/)
- [Web.dev - Use imagemin to compress images](https://web.dev/use-imagemin-to-compress-images/) 