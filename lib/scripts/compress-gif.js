const imagemin = require('imagemin')
const imageminGiflossy = require('imagemin-giflossy')

const compressAsync = (srcDir, destDir) =>
  imagemin([`${srcDir}/*.gif`], {
    destination: destDir,
    plugins: [imageminGiflossy({ lossy: 80 })],
  })

module.exports = compressAsync