const imagemin = require('imagemin')
const imageminMozjpeg = require('imagemin-mozjpeg')

const compressAsync = (srcDir, destDir) =>
  imagemin([`${srcDir}/*.jpg`], {
    destination: destDir,
    plugins: [imageminMozjpeg()],
  })

module.exports = compressAsync