const imagemin = require('imagemin')
const imageminPngquant = require('imagemin-pngquant')

const compressAsync = (srcDir, destDir) =>
  imagemin([`${srcDir}/*.png`], {
    destination: destDir,
    plugins: [imageminPngquant()],
  })

module.exports = compressAsync