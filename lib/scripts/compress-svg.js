const imagemin = require('imagemin')
const imageminSvgo = require('imagemin-svgo')
const { extendDefaultPlugins } = require('svgo')

const compressAsync = (srcDir, destDir) =>
  imagemin([`${srcDir}/*.svg`], {
    destination: destDir,
    plugins: [
      imageminSvgo({
        plugins: extendDefaultPlugins([{ name: 'removeViewBox', active: false }]),
      }),
    ],
  })

module.exports = compressAsync