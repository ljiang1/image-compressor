/**
 * It uses node module `imagemin` and its plugins to compress
 * assets file formats: gif, jpg, png and svg. One of the plugins
 * for jpg requires additional system libraries to build from source.
 * If you are using a docker container from node alpine image, you can
 * run the following commnands to install the system libraries:
 * - apk update
 * - apk add autoconf libtool nasm libpng-dev automake pkgconfig build-base
 */
const { resolve } = require('path')
const compressGifAsync = require('./compress-gif');
const compressJpgAsync = require('./compress-jpg');
const compressPngAsync = require('./compress-png');
const compressSvgAsync = require('./compress-svg');

// Get asset path
const LIB_ROOT = resolve(__dirname, '..')
const SRC_PATH = resolve(LIB_ROOT, 'src')
const DEST_PATH = resolve(LIB_ROOT, 'dest')

Promise.all([
  compressGifAsync(SRC_PATH, DEST_PATH),
  compressJpgAsync(SRC_PATH, DEST_PATH),
  compressPngAsync(SRC_PATH, DEST_PATH),
  compressSvgAsync(SRC_PATH, DEST_PATH),
])
  .then(() => {
    console.log('Images optimized')
  })
  .catch((error) => {
    console.error('Error in image optimization', error)
  })
